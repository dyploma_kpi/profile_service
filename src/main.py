from contextlib import asynccontextmanager
from src.config import settings

from fastapi import (FastAPI,
                     status,
                     Depends,
                     Header,
                     Query,
                     Path,
                     UploadFile,
                     File)
from fastapi.middleware.cors import CORSMiddleware

from src.service import ( profile_service,
                          ProfileService,
                          review_service,
                          ReviewService,
                          file_service,
                          FileService)

from src.models.schems import ( CreateProfile,
                                UpdateProfile,
                                CreateReview,
                                InfoProfile,
                                InfoReview,
                                InfoFile)

def get_profile_service() -> ProfileService:
    return profile_service

def get_review_service() -> ReviewService:
    return review_service

def get_file_service() -> FileService:
    return file_service

@asynccontextmanager
async def lifespan(app: FastAPI):
    yield
    
    
app = FastAPI(lifespan=lifespan, openapi_prefix=settings.api_prefix)

origins = [
    "http://localhost:4200",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("", status_code=status.HTTP_201_CREATED)
async def create_profile(
    profile: CreateProfile, 
    profile_srv: ProfileService = Depends(get_profile_service)
):
    if await profile_srv.create_profile(profile):
        return {"message": "Profile created successfully"}
    
@app.patch("", response_model=InfoProfile, status_code=status.HTTP_200_OK)
async def update_profile(update_profile: UpdateProfile, 
                         auth: str = Header(None), 
                         profile_srv: ProfileService = Depends(get_profile_service),
                        ):
    return await profile_srv.update_profile(update_profile, auth)
    
@app.get("", response_model=list[InfoProfile], status_code=status.HTTP_200_OK)
async def get_profiles(offset: int = Query(1, ge=1),
                       size: int = Query(12, ge=1),
                       auth: str = Header(None),
                       profile_srv: ProfileService = Depends(get_profile_service)
                       ):
    return await profile_srv.get_profiles(offset, size, auth)
    
@app.get("/my", status_code=status.HTTP_200_OK)
async def get_my_profile(auth: str = Header(None),
                         profile_srv: ProfileService = Depends(get_profile_service)
                         ):
    return await profile_srv.get_myself_profile(auth)
    
@app.get("/{profile_id}", response_model=InfoProfile, status_code=status.HTTP_200_OK)
async def get_profile_by_profile_id(profile_id: int = Path(ge=1),
                                 auth: str = Header(None),
                                 profile_srv: ProfileService = Depends(get_profile_service)
                                 ):
    return await profile_srv.get_profile_by_profile_id(profile_id, auth)

@app.post("/review", status_code=status.HTTP_201_CREATED)
async def create_review(create_review: CreateReview,
                        auth: str = Header(None),
                        review_srv: ReviewService = Depends(get_review_service)):
    if await review_srv.create_review(create_review, auth):
        return {"message": "Review created successfully"}

@app.get("/review/my", response_model=list[InfoReview], status_code=status.HTTP_200_OK)
async def get_reviews_by_profile_id(offset: int = Query(0, ge=1),
                                    size: int = Query(12, ge=1),
                                    auth: str = Header(None),
                                    review_srv: ReviewService = Depends(get_review_service)):
    return await review_srv.get_my_reviews(offset, size, auth)

@app.get("/review/{profile_id}", response_model=list[InfoReview], status_code=status.HTTP_200_OK)
async def get_reviews_by_profile_id(profile_id: int = Path(ge=1),
                                    offset: int = Query(1, ge=1),
                                    size: int = Query(12, ge=1),
                                    auth: str = Header(None),
                                    review_srv: ReviewService = Depends(get_review_service)):
    return await review_srv.get_reviews(offset, size, profile_id, auth)

@app.post("/file/avatar", response_model=InfoProfile,  status_code=status.HTTP_201_CREATED)
async def upload_avatar(auth: str = Header(None),
                        file: UploadFile = File(...),
                        file_srv: FileService = Depends(get_file_service)):
    return await file_srv.modify_avatar(file, auth)

@app.post("/file/desc", response_model=list[InfoFile], status_code=status.HTTP_201_CREATED)
async def upload_desc_files(files: list[UploadFile],
                            auth: str = Header(None),
                            file_srv: FileService = Depends(get_file_service)):
    return await file_srv.upload_desc_files(files, auth)

@app.delete("/file/desc/{file_id}", status_code=status.HTTP_200_OK)
async def delete_desc_file(file_id: int = Path(qe=1),
                            auth: str = Header(None),
                            file_srv: FileService = Depends(get_file_service)):
    if await file_srv.delete_desc_file(file_id, auth):
        return {"message": "Delete successfully"}
    
@app.get("/file/desc/my", status_code=status.HTTP_200_OK)
async def get_my_desc_files(auth: str = Header(None),
                            file_srv: FileService = Depends(get_file_service)):
    return await file_srv.get_my_desc_files(auth)
    
@app.get("/file/desc/{profile_id}", status_code=status.HTTP_200_OK)
async def gey_desc_files_by_profile_id(profile_id: int = Path(qe=1),
                            auth: str = Header(None),
                            file_srv: FileService = Depends(get_file_service)):
    return await file_srv.get_desc_files_by_profile_id(profile_id, auth)
