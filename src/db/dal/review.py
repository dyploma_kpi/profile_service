from sqlalchemy import select
from sqlalchemy.engine import Result
from sqlalchemy.orm import joinedload, selectinload

from src.models.dal import Review
from src.db import DatabaseHelper, db_helper

class ReviewDAL():
    
    def __init__(self, db_helper: DatabaseHelper) -> None:
        self._db_helper = db_helper
        

    async def create_review(self, author_id: int, profile_id: int, rate: int, desc: str) -> int | None:
        async with self._db_helper.session_factory() as session:
            try:
                review = Review(author_id=author_id, profile_id=profile_id, rate=rate, desc=desc)
                session.add(review)
                await session.commit()
                await session.refresh(review) 
                return review.id
            except Exception as e:
                print(f"Error creating review: {e}")
                return None
            
    async def get_reviews(self, profile_id: int) -> list[Review]:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(Review).options(joinedload(Review.author)).where(Review.profile_id == profile_id)
                reviews: list[Review] | None = await session.scalars(stmt)

                await session.commit()
                return reviews
            except Exception as e:
                print(f"Error get review: {e}")
                return None


review_dal = ReviewDAL(db_helper)
    