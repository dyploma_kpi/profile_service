from sqlalchemy import select
from sqlalchemy.engine import Result
from sqlalchemy.orm import joinedload, selectinload

from src.models.dal import Profile
from src.db import DatabaseHelper, db_helper
from src.models.schems import UpdateProfile

class ProfileDAL():
    
    def __init__(self, db_helper: DatabaseHelper) -> None:
        self._db_helper = db_helper
        

    async def create_profile(self, user_id: int, first_name: str, last_name: str) -> bool:
        async with self._db_helper.session_factory() as session:
            try:
                profile = Profile(id=user_id, first_name=first_name, last_name=last_name)
                session.add(profile)
                await session.commit()
                return True
            except Exception as e:
                print(f"Error create profile: {e}")
                return False
            
    async def update_profile(self, user_id: int, update_profile: UpdateProfile) -> Profile | None:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(Profile).where(Profile.id == user_id)
                profile: Profile | None = await session.scalar(stmt)
                if not profile:
                    return None

                for key, value in update_profile.model_dump(exclude_unset=True).items():
                    setattr(profile, key, value)

                await session.commit()
                return profile
            except Exception as e:
                print(f"Error update profile: {e}")
                return None
            
    async def update_avatar_url(self, user_id: int, avatar_url: str) -> Profile | None:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(Profile).where(Profile.id == user_id)
                profile: Profile | None = await session.scalar(stmt)
                if not profile:
                    return None

                setattr(profile, "avatar_url", avatar_url)

                await session.commit()
                await session.refresh(profile)
                return profile
            except Exception as e:
                print(f"Error update profile: {e}")
                return None
            
    async def get_profiles(self, user_id: int) -> list[Profile] | None:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(Profile).where(Profile.id != user_id)
                profiles: list[Profile] | None = await session.scalars(stmt)

                await session.commit()
                return profiles
            except Exception as e:
                print(f"Error get profiles: {e}")
                return None
            
    async def get_profile(self, id: int) -> Profile | None:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(Profile).where(Profile.id == id)
                profile: Profile | None = await session.scalar(stmt)

                await session.commit()
                return profile
            except Exception as e:
                print(f"Error get profile by profile_id: {e}")
                return None
            
    async def get_profile_by_avatar_url(self, url: str) -> Profile | None:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(Profile).where(Profile.avatar_url == url)
                profile: Profile | None = await session.scalar(stmt)

                await session.commit()
                return profile
            except Exception as e:
                print(f"Error get profile by avatar_url: {e}")
                return None


profile_dal = ProfileDAL(db_helper)
    