from typing import TYPE_CHECKING
from sqlalchemy import Integer, Text, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.models.dal.base import Base

if TYPE_CHECKING:
    from src.models.dal import Profile


class Review(Base):
    author_id: Mapped[int] = mapped_column(ForeignKey("profiles.id"), unique=False, nullable=False)
    profile_id: Mapped[int] = mapped_column(ForeignKey("profiles.id"), unique=False, nullable=False)
    rate: Mapped[int] = mapped_column(Integer, nullable=False)
    desc: Mapped[str] = mapped_column(Text, nullable=False)

    profile: Mapped["Profile"] = relationship("Profile", back_populates="reviews", foreign_keys=[profile_id])
    author: Mapped["Profile"] = relationship("Profile", back_populates="author_reviews", foreign_keys=[author_id])
