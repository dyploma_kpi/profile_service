from typing import TYPE_CHECKING

from sqlalchemy import String, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.models.dal.base import Base

if TYPE_CHECKING:
    from src.models.dal import Profile


class File(Base):
    profile_id: Mapped[int] = mapped_column(ForeignKey("profiles.id"), unique=False, nullable=False)
    file_url: Mapped[str] = mapped_column(String(300), nullable=False)
    
    profile: Mapped["Profile"] = relationship("Profile", back_populates="files")
