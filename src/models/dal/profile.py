from typing import TYPE_CHECKING
from sqlalchemy import String, DateTime, Integer, Text
import datetime
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.models.dal.base import Base
from src.config import settings

if TYPE_CHECKING:
    from src.models.dal import File
    from src.models.dal import Review


class Profile(Base):
    first_name: Mapped[str] = mapped_column(String(100), nullable=False)
    last_name: Mapped[str] = mapped_column(String(100), nullable=False)
    country: Mapped[str] = mapped_column(String(100), nullable=True)
    city: Mapped[str] = mapped_column(String(100), nullable=True)
    expirience_year: Mapped[int] = mapped_column(Integer, nullable=True)
    expirience_type: Mapped[str] = mapped_column(String(100), nullable=True)
    expirience_desc: Mapped[str] = mapped_column(Text, nullable=True)
    graduation_type: Mapped[str] = mapped_column(String(100), nullable=True)
    avatar_url: Mapped[str] = mapped_column(String(300), nullable=False, server_default=settings.s3_default_avatar)
    modification_datetime: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), onupdate=func.now(), server_default=func.now())

    files: Mapped[list["File"]] = relationship(back_populates="profile")
    reviews: Mapped[list["Review"]] = relationship(back_populates="profile", foreign_keys="Review.profile_id")
    author_reviews: Mapped[list["Review"]] = relationship(back_populates="author", foreign_keys="Review.author_id")
