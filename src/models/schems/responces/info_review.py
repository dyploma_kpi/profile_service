from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, Field
import datetime

from src.models.schems import ReviewAuthor


class InfoReview(BaseModel):
    rate: Annotated[int, Field(ge=1, le=4)]
    desc: Annotated[str, MinLen(10), MaxLen(300)]
    date: datetime.datetime
    author: ReviewAuthor
