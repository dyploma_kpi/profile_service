from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, Field


class ReviewAuthor(BaseModel):
    id: Annotated[int, Field(ge=1)]
    first_name: Annotated[str, MinLen(3), MaxLen(100)]
    avatar_url: Annotated[str, MinLen(15), MaxLen(1000)]
