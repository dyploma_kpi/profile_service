from typing import Annotated
from annotated_types import MaxLen
from pydantic import BaseModel, Field


class InfoFile(BaseModel):
    id: Annotated[int, Field(ge=1)]
    profile_id: Annotated[int, Field(ge=1)]
    file_url: Annotated[str, MaxLen(300)]
