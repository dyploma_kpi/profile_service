from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, Field


class InfoProfile(BaseModel):
    id: Annotated[int, Field(ge=1)]
    first_name: Annotated[str, MinLen(3), MaxLen(100)]
    last_name: Annotated[str, MinLen(3), MaxLen(100)]
    country: Annotated[str, MaxLen(100)] | None = None
    city: Annotated[str, MaxLen(100)] | None = None
    expirience_year: Annotated[int, Field(gt=1)] | None = None
    expirience_type: Annotated[str, MaxLen(100)] | None = None
    expirience_desc: Annotated[str, MaxLen(1000)] | None = None
    graduation_type: Annotated[str, MaxLen(100)] | None = None
    avatar_url: Annotated[str, MinLen(15), MaxLen(1000)]
