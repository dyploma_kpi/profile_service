from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, Field


class UpdateProfile(BaseModel):
    first_name: Annotated[str, MinLen(3), MaxLen(100)] | None = None
    last_name: Annotated[str, MinLen(3), MaxLen(100)] | None = None
    country: Annotated[str, MaxLen(100)] | None = None
    city: Annotated[str, MaxLen(100)] | None = None
    expirience_year: Annotated[int, Field(gt=1)] | None = None
    expirience_type: Annotated[str, MaxLen(100)] | None = None
    expirience_desc: Annotated[str, MaxLen(1000)] | None = None
    graduation_type: Annotated[str, MaxLen(100)] | None = None
