from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, Field


class CreateProfile(BaseModel):
    id: int = Field(..., ge=1)
    first_name: Annotated[str, MinLen(3), MaxLen(100)]
    last_name: Annotated[str, MinLen(3), MaxLen(100)]

