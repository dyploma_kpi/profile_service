import os
from pathlib import Path
from pydantic_settings import BaseSettings, SettingsConfigDict
from pydantic import Field

BASE_DIR = Path(__file__).parent.parent


class Settings(BaseSettings):
    api_prefix: str = "/api/v1/profile"
    db_user: str = Field(..., env="DB_USER")
    db_pass: str = Field(..., env="DB_PASS")
    db_host: str = Field(..., env="DB_HOST")
    db_port: str = Field(..., env="DB_PORT")
    db_name: str = Field(..., env="DB_NAME")
    db_echo: bool = True
    s3_url: str = Field(..., env="S3_URL")
    s3_avatar_dir: str = Field("avatars", env="S3_AVATAR_DIR")
    s3_desc_dir: str = Field("desc", env="S3_DESC_DIR")
    s3_default_avatar: str = "https://drone-guy.fra1.digitaloceanspaces.com/avatars/avatar.jpeg"
    s3_access_key: str = Field(..., env="S3_ACCESS_KEY")
    s3_secret_key: str = Field(..., env="S3_SECRET_KEY")
    s3_region_name: str = "fra1"
    s3_space_name: str = "drone-guy"
    auth_api_url: str = "http://fastapi-auth:8000/api/v1/auth"
    
    
    model_config = SettingsConfigDict(env_file="/Users/macuser/University/diploma/profile_service/.env")

    @property
    def db_url(self) -> str:
        return (
            f"mysql+aiomysql://{self.db_user}:{self.db_pass}@"
            f"{self.db_host}:{self.db_port}/{self.db_name}"
        )
        
    @property
    def db_url_for_alembic(self) -> str:
        return (
            f"mysql+mysqlconnector://{self.db_user}:{self.db_pass}@"
            f"{self.db_host}:{self.db_port}/{self.db_name}"
        )


settings = Settings()
