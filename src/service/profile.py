import httpx
import json
from src.db import ProfileDAL, profile_dal
from src.config import settings
from src.models.schems import CreateProfile, UpdateProfile, InfoProfile
from src.models.dal import Profile
from src.utils import Verificator, verificator

from pydantic import parse_obj_as

from fastapi import HTTPException, status


class ProfileService():
    def __init__(self, profile_dal: ProfileDAL, verificator: Verificator) -> None:
        self._profile_dal: ProfileDAL = profile_dal
        self._verificator: Verificator = verificator
        
    async def create_profile(self, profile: CreateProfile) -> bool:
        if not await self._profile_dal.create_profile(profile.id, profile.first_name, profile.last_name):
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        return True
    
    async def update_profile(self, update_profile: UpdateProfile, token: str) -> InfoProfile:
        user_id: int = await self._verificator.verify(token)
        profile: Profile | None = await self._profile_dal.update_profile(user_id, update_profile)
        if not profile:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        return parse_obj_as(InfoProfile, vars(profile))
    
    async def get_profiles(self, offset: int, size: int, token: str) -> list[InfoProfile]:
        user_id: int = await self._verificator.verify(token)
        profiles: list[Profile] | None = await self._profile_dal.get_profiles(user_id)
        return parse_obj_as(list[InfoProfile], [vars(profile) for profile in profiles])[offset-1:offset-1+size]
    
    async def get_profile_by_profile_id(self, profile_id, token: str) -> InfoProfile:
        user_id: int = await self._verificator.verify(token)
        if user_id == profile_id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Use another request to get information about yourself"
            )
        profile: Profile | None = await self._profile_dal.get_profile(profile_id)
        if not profile:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Profile didn`t find"
            )
        return parse_obj_as(InfoProfile, vars(profile))
    
    async def get_myself_profile(self, token: str) -> InfoProfile:
        user_id: int = await self._verificator.verify(token)
        profile: Profile | None = await self._profile_dal.get_profile(user_id)
        if not profile:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Profile didn`t find"
            )
        return parse_obj_as(InfoProfile, vars(profile))
    
    
        
        
    
        
profile_service = ProfileService(profile_dal, verificator)
