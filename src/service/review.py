from src.db import ReviewDAL, review_dal
from src.utils import Verificator, verificator
from src.models.schems import CreateReview, InfoReview, ReviewAuthor
from src.models.dal import Review

from fastapi import HTTPException, status
from pydantic import parse_obj_as


class ReviewService():
    def __init__(self, review_dal: ReviewDAL, verificator: Verificator) -> None:
        self._review_dal: ReviewDAL = review_dal
        self._verificator: Verificator = verificator
        
    async def create_review(self, create_review: CreateReview, token: str) -> bool:
        user_id: int = await self._verificator.verify(token)
        if user_id == create_review.profile_id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="You can`t make review for yourself"
            )
        if not await self._review_dal.create_review(user_id, create_review.profile_id, create_review.rate, create_review.desc):
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        return True
    
    async def get_reviews(self, offset: int, size: int, profile_id: int, token: str) -> list[InfoReview]:
        user_id: int = await self._verificator.verify(token)
        if user_id == profile_id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Use another request to get information about yourself"
            )
        reviews: list[Review] = await self._review_dal.get_reviews(profile_id)
        if not reviews:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
            
        reviews_schemas: list[InfoReview] = []
        for review in reviews:
            reviews_schemas.append(InfoReview(
                rate=review.rate,
                desc=review.desc,
                author=ReviewAuthor(
                    id=review.author.id,
                    first_name=review.author.first_name,
                    avatar_url=review.author.avatar_url
                )
            ))
        
        return reviews_schemas[offset-1:offset-1+size]
    
    async def get_my_reviews(self, offset: int, size: int, token: str) -> list[InfoReview]:
        user_id: int = await self._verificator.verify(token)
        reviews: list[Review] = await self._review_dal.get_reviews(user_id)
        if not reviews:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        reviews_schemas: list[InfoReview] = []
        for review in reviews:
            reviews_schemas.append(InfoReview(
                rate=review.rate,
                desc=review.desc,
                date=review.creation_datetime,
                author=ReviewAuthor(
                    id=review.author.id,
                    first_name=review.author.first_name,
                    avatar_url=review.author.avatar_url
                )
            ))
        
        return reviews_schemas[offset-1:offset-1+size]
        
        
review_service = ReviewService(review_dal, verificator)
